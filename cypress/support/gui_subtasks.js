Cypress.Commands.add('gui_createSubTask', (subtask) => {
    cy.visit('/tasks')

//See if the menssage of Max characters appear
//See if the menssage of The Subtask Description and Due Date are required fields
    cy.contains('(0) Manage Subtasks').click()   
    cy.get('#edit_task').type('123')
    cy.get('#new_sub_task').type(subtask.despriptionOutMax)
    cy.get('#add-subtask').click()
    cy.contains('The field accept between 3 and 250').should('exist')
    cy.contains('The field Description and Date are required fields').should('exist')
    cy.get('#new_sub_task').clear() 
 
//See if the menssage of The Subtask Description and Due Date are required fields
//See if the menssage Due Date field just accept today or more with 'MM/dd/yyyy' format
    cy.get('#dueDate').clear()
    cy.get('#dueDate').type(Date.pastyear)
    cy.get('#add-subtask').click()
    cy.contains('The field Description and Date are required fields').should('exist')


 //The addition and validation of a new SubTask and a validation for modal dialog

    cy.get('#new_sub_task').type(subtask.despriptionMax)
    cy.get('#dueDate').clear()
    cy.get('#dueDate').type(Date.today)
    cy.get('#add-subtask').click()
    cy.contains(subtask.despriptionMax).should('exist')
    cy.get('#my_task').should('exist')

 //The new SubTask showed on 'My Task'
    cy.contains('Close').click()
    cy.contains('(1) Manage Subtasks').should('exist')

//Remore SubTask
   cy.contains('(1) Manage Subtasks').click()
   cy.contains('Remove SubTask').click()
   cy.contains('(0) Manage Subtasks').should('exist')


});