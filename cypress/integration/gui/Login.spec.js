/// <reference types="Cypress" />

const faker = require('faker')

describe('Login', () => {
    const register = {
        name: `register-${faker.datatype.uuid()}`,
        password: faker.random.words(5),
        email: faker.internet.email()
    }
    beforeEach(() => cy.gui_Register(register))  

    it('successfully', () => {

        cy.login(register)

        cy.get('#my_task').should('exist')
        cy.contains(`Hey ${register.name}, this is your todo list for today`).should('exist')
    });
});