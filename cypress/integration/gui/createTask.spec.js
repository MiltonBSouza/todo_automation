/// <reference types="Cypress" />

const faker = require('faker')

describe('CreateTask', () => {

    beforeEach(() => cy.login_easy())
    
    it('successfully', () => {
        const task = {
            name: `task-${faker.datatype.uuid()}`,
            despriptionOutMax: faker.random.words(251),
            despriptionMax: faker.random.words(250),
            despriptionOutMinimum: faker.random.words(2),
            despriptionMinimum: faker.random.words(3),
            email: faker.internet.email()
        }

        cy.gui_createTask(task)  
        
        cy.get('#my_task').should('exist')
    })
    })