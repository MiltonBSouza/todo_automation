//Easy Login for tests
Cypress.Commands.add('login_easy', () => {
    cy.visit('users/sign_in')

    cy.get('#user_email').type(Cypress.env('user_name'))
    cy.get('#user_password').type(Cypress.env('user_password'))
    cy.get('#user_password').type('{enter}')
 
});

//Make a Login with a account
Cypress.Commands.add('login', (register) => {
    cy.visit('users/sign_in')

    cy.get('#user_email').type(register.email) 
    cy.get('#user_password').type(register.password)
    cy.get('#user_password').type('{enter}')
    cy.get('#my_task').click()
 
});

//Make a Logout
Cypress.Commands.add('logout', () => {

    cy.contains('Sign out').click()


});

//Make a Register of User
Cypress.Commands.add('gui_Register', (register) => {
    cy.visit('/users/sign_up')

    cy.get('#user_name').type(register.name)
    cy.get('#user_email').type(register.email)   
    cy.get('#user_password').type(register.password)
    cy.get('#user_password_confirmation').type(register.password)
    cy.get('#user_password').type('{enter}')
    cy.contains('Sign out').click()

});