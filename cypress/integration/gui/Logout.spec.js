/// <reference types="Cypress" />

describe('Logout', () => {

beforeEach(() => cy.login())

    it('successfully', () => {
        cy.logout()

        cy.contains('Sign In').should('exist')
    })
})