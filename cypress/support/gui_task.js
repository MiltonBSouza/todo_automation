Cypress.Commands.add('gui_createTask', (task) => {
    cy.visit('/tasks')

//See if the menssage of Max and Minimum characters appear
    cy.get('#new_task').type(task.despriptionOutMax)
    cy.get('.glyphicon-plus').click()
    cy.contains('The field accept between 3 and 250').should('exist')    
    cy.get('#new_task').clear()

//The field should be accept this characters
    cy.get('#new_task').type(task.despriptionMax)
    cy.get('#new_task').type('{enter}')
    cy.contains(task.despriptionMax).should('exist')

//See if the menssage of Max and Minimum characters appear
    cy.get('#new_task').type(task.despriptionOutMinimum)
    cy.get('.glyphicon-plus').click()
    cy.contains('The field accept between 3 and 250').should('exist')     
    cy.get('#new_task').clear()

//The field should be accept this characters
    cy.get('#new_task').type(task.despriptionMinimum)
    cy.get('.glyphicon-plus').click()
    cy.contains(task.despriptionMinimum).should('exist')

//Remove a Task
    cy.contains('Remove').click()

});