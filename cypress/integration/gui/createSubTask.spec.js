/// <reference types="Cypress" />

const faker = require('faker')
const dayjs = require('dayjs')

describe('CreateSubTask', () => {

    beforeEach(() => cy.login_easy())
    
    it('successfully', () => {
        const subtask = {
            name: `task-${faker.datatype.uuid()}`,
            despriptionOutMax: faker.random.words(251),
            despriptionMax: faker.random.words(250),
            despriptionOutMinimum: faker.random.words(2),
            despriptionMinimum: faker.random.words(3),
        }
        const Date = {
            today: dayjs().format('MM/DD/YYYY'),
            newyear: dayjs().add(1, 'year').format('MM/DD/YYYY'),
            pastyear: dayjs().add(-1, 'year').format('MM/DD/YYYY'),
            otherformat: dayjs().format('DD/MM/YYYY'),
        }

        cy.gui_createSubTask(subtask, Date)  
        cy.get('#my_task').should('exist')
    })
    })