/// <reference types="Cypress" />

const faker = require('faker')

describe('Register', () => {

    it('successfully', () => {
        const task = {
            name: `task-${faker.datatype.uuid()}`,
            despription: faker.random.words(5),
            email: faker.internet.email()
        }

        cy.gui_Register(task)  
        cy.get('#my_task').should('exist')
    })

})